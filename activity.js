//Baron Jon A. Gascon

function oddEvenChecker(number){
	if(typeof number === 'number'){
		if(number % 2 == 0){
			return "The number is even.";
		} else if (number % 2 != 0){
			return "The number is odd.";
		};
	} else {
		alert("Invalid Input.");
	};	
};

console.log(oddEvenChecker(12));

function budgetChecker(budget){
	if(typeof budget === 'number'){
		if(budget > 40000){
			return "You are over the budget.";
		} else if(budget < 40000){
			return "You have resources left.";
		};
	} else{
		alert("Invalid Input.");
	};
};

console.log(budgetChecker(43000));
