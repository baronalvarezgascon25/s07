/* Conditionial Statement 
 * -key features of a programming language
 */

let variable = "initial value";

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

let sum = num1 + num4;

num1 = num1 + num4;

console.log(num1);

num2 += num3;
console.log(num2);

num1 += 55;
console.log(num1);
console.log(num4);

let string1 = "Boston";
let string2 = "Celtics";

string1 += string2;
console.log(string1);

console.log(string2);

num1 -= num2;
console.log("Result of minus operator "+num1);

num2 *= num3;
console.log("Result of multiplication operator "+num2);

num4 /= num3;
console.log(num4);

let x = 1397;
let y = 7831;

sum = x+y;
console.log("Result of addition operator "+sum);

let difference = y-x;
console.log("Result of subtraction operator "+difference);

let product = y*x;
console.log("Result of multiplication operator "+product);

let quotient = y/x;
console.log("result of division operator "+quotient);

let remainder = y%x;
console.log("result of modulo operator "+remainder);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: "+mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: "+pemdas);

pemdas = (1+(2-3))*(4/5);
console.log("Result of 2nd pemdas operation: "+pemdas);

let z = 1;

++z;
console.log("Pre-fix increment: "+z);
z++;
console.log("Post-fix increment: "+z);

console.log(z++);

console.log(z);

console.log(z++);

console.log(z);

console.log(++z);

console.log(--z);

console.log(z--);

console.log(z);

//Comparison operators
console.log(1 == 1);

let izSame = 55 == 55;
console.log(izSame);

console.log(1 == '1');

console.log(0 == false);

console.log(1 == true);

console.log("false" == 0);

console.log(true == "true");

console.log(true == "1");

console.log(true === '1');

console.log("BTS" === "BTS");

console.log("Marie" === "marie");

// Inequality operators

console.log('1' != 1);

console.log("5" !== 5);

console.log(5 !== 5);

let name1 = "Jin";
let name2 = "jimin";
let name3 = "Jungkook";
let name4 = "v";

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1); //true
console.log(numString1 === number1); //false
console.log(numString1 != number1); //false
console.log(name4 !== "num3"); //true
console.log(name3 == "Jungkook"); //true
console.log(name1 === "Jin"); //true

let a = 500;
let b = 700;
let c = 8000;
let numString3 = "5500";

console.log(a > b);
console.log(c > y);

console.log(c < a);
console.log(b < b);
console.log(a < 1000);
console.log(numString3 < 1000);
console.log(numString3 < 6000);
console.log(numString3 < "Jose");

console.log(c >= 10000);
console.log(b >= a);

console.log(a <= b);
console.log(c <= a);

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3);

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4);

let userName = "gamer2001";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;

console.log(registration1);

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirement1);

let guildRequirement2 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement2);

let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin2);
console.log(!isRegistered);

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("Welcome to Game Online!");
};
if(userLevel3 >= requiredLevel){
	console.log("You are qualified to join the guild.");
};
if(userName3.length > 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the Noobies guild!!!");
} else {
	console.log("You are too strong to be a noob...");
};

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild");
} else if(userLevel3 > 25){
	console.log("You are too strong to be a noob");
} else if(userAge3 < requiredAge){
	console.log("You are too young to join the guild");
} else if(userName3.length < 10){
	console.log("Username too short");
};

function addNum(num1, num2){
	if(typeof num1 === 'number' && typeof num2 === 'number'){
		console.log("run only if both arguments passed are number types");
		console.log(num1 + num2);
	} else {
		console.log("one or both arguments are not numbers");
	};
};

addNum(5,"2");

function login(username, password){
	if(typeof username === 'string' && typeof password === 'string'){
		console.log("Both arguments are strings.");
		let isNameString = true;
		let isPasswordString = true;
		if(isNameString == true || isPasswordString == true){
			if(password.length >= 8 && username.length >= 8){
				console.log("Welcome to Game Online!");
			} else if(password.length <= 7){
				alert("Password is too short")		
			} else if(username.length <= 7){
				alert("Username is too short")
			};
			console.log("run if the parent if statement is able to agree to accomplish its condition.");		
		};
	};		
};


//Baron Jon A. Gascon

function shirtColor(day){
	let colorOfTheDay;
	let dayToday = day.toLowerCase();
	if(typeof dayToday === 'string'){

		if(dayToday == "monday"){
			colorOfTheDay = "Black";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else if(dayToday == "tuesday"){
			colorOfTheDay = "Green";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else if(dayToday == "wednesday"){
			colorOfTheDay = "Yellow";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else if(dayToday == "thursday"){
			colorOfTheDay = "Red";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else if(dayToday == "friday"){
			colorOfTheDay = "Violet";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else if(dayToday == "saturday"){
			colorOfTheDay = "Blue";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else if(dayToday == "sunday"){
			colorOfTheDay = "White";
			alert("Today is "+dayToday+", Wear "+colorOfTheDay);
		} else {
			alert("Invalid input. Enter a valid day of the week.");
		}

	} else if(typeof dayToday === 'number'){
		alert("Invalid Input. Please input a string.");
	};
}

shirtColor("Wednesday");